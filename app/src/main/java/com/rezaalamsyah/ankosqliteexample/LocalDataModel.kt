package com.rezaalamsyah.ankosqliteexample

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Identitas(val id: Long?, val id_identitas: String?, val nama: String?, val alamat: String?) : Parcelable{
    companion object {
        const val TABLE_IDENTITAS: String = "TABLE_IDENTITAS"
        const val ID : String = "ID"
        const val ID_IDENTITAS : String = "ID_IDENTITAS"
        const val NAMA : String = "NAMA"
        const val ALAMAT : String = "ALAMAT"
    }
}