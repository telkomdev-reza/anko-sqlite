package com.rezaalamsyah.ankosqliteexample

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.rezaalamsyah.ankosqliteexample.R.layout.adapter_identitas
import kotlinx.android.synthetic.main.adapter_identitas.view.*
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk25.coroutines.onClick

class AdapterIdentity(private val context: Context,
                      private val identitas: List<Identitas>,
                      private val listener : (Identitas)->Unit,
                      private val listenerDelete : (TextView)->Unit):
        RecyclerView.Adapter<AdapterIdentity.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(context).inflate(adapter_identitas, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(identitas[position], listener,listenerDelete)
    }

    override fun getItemCount(): Int {
        return identitas.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindItem(items: Identitas, listener: (Identitas) -> Unit,listenerDelete: (TextView) -> Unit) {
            itemView.txtNama.text = items.nama
            itemView.txtAlamat.text = items.alamat
            itemView.onClick{
                listener(items)
            }

            itemView.btnHapus.onClick {
                listenerDelete(itemView.btnHapus)
            }
        }
    }
}