package com.rezaalamsyah.ankosqliteexample

import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.rezaalamsyah.ankosqliteexample.R.layout.*
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.util.*

class MainActivity : AppCompatActivity() {

    private  var dataLocalIdentitas: MutableList<Identitas> = mutableListOf()
    private lateinit var strNama : String
    private lateinit var strAlamat : String
    private var randomNum : Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_main)
        initView()
        buttonAction()
        showDataFromSQLite()
    }

    private fun buttonAction(){
        btnTambah.onClick {
            addDataToSQLite()
            showDataFromSQLite()
        }
    }

    private fun getData(){
        strNama = nama.text.toString()
        strAlamat = alamat.text.toString()
    }

    private fun initView(){
        rvIdentitas.layoutManager = LinearLayoutManager(this)
        rvIdentitas.adapter = AdapterIdentity(this, dataLocalIdentitas,{
            startActivity<DetailActivity>(
                "key_data" to it
            )
        },{
            deleteDataFromSQLite()
        })
    }

    private fun addDataToSQLite(){
        getData()
        val rand = Random()
        randomNum = rand.nextInt(50) + 1
        try {
            database.use {
                insert(Identitas.TABLE_IDENTITAS,
                        Identitas.ID_IDENTITAS to randomNum.toString(),
                        Identitas.NAMA to strNama,
                        Identitas.ALAMAT to strAlamat)
            }
            toast("Berhasil tambah data")
        } catch (e: SQLiteConstraintException){
        }
    }

    private fun showDataFromSQLite(){
        database.use {
            val result = select(Identitas.TABLE_IDENTITAS)
            val dataLocal= result.parseList(classParser<Identitas>())
            dataLocalIdentitas.addAll(dataLocal)
            rvIdentitas.adapter.notifyDataSetChanged()
        }
    }

    private fun deleteDataFromSQLite(){
        try {
            database.use {
                delete(Identitas.TABLE_IDENTITAS, "(ID_IDENTITAS = {id})",
                        "id" to randomNum.toString())
            }
            toast("Berhasil hapus data")
            dataLocalIdentitas.clear()
            showDataFromSQLite()
        } catch (e: SQLiteConstraintException){

        }
    }
}
