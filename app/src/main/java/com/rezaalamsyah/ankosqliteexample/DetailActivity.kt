package com.rezaalamsyah.ankosqliteexample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.rezaalamsyah.ankosqliteexample.R.layout.activity_detail
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {
    private lateinit var dataIdentitas: Identitas

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_detail)
        getDataFromIntent()
    }

    private fun getDataFromIntent(){
        val intent = intent
        dataIdentitas = intent.extras.get("key_data") as Identitas
        detailNama.text = dataIdentitas.nama
        detailAlamat.text = dataIdentitas.alamat
    }
}
