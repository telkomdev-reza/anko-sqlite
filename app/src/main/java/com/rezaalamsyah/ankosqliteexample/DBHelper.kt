package com.rezaalamsyah.ankosqliteexample

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class DBHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "DbIdentitas.db", null, 1){
    companion object {
        private var instance: DBHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DBHelper{
            if (instance == null) {
                instance = DBHelper(ctx.applicationContext)
            }
            return instance as DBHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        // Here you create tables
        db.createTable(Identitas.TABLE_IDENTITAS, true,
                Identitas.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                Identitas.ID_IDENTITAS to TEXT,
                Identitas.NAMA to TEXT,
                Identitas.ALAMAT to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Here you can upgrade tables, as usual
        db.dropTable(Identitas.TABLE_IDENTITAS, true)
    }
}

// Access property for Context
val Context.database: DBHelper
    get() = DBHelper.getInstance(applicationContext)
